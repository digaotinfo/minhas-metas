import { useSQLiteContext } from "expo-sqlite/next";

export type GoalCreateDatabase = {
    name: string;
    total: number;
}

export type GoalResponseDatabas = {
    id: string;
    name: string;
    total: number;
    current: number;
}

export function useGoalRepository() {
    const database = useSQLiteContext();

    function create(goal: GoalCreateDatabase) {

        try {
            const statement = database.prepareSync(
                "INSERT INTO goals (name, total) VALUES ($name, $total)"
            );
            statement.executeAsync({
                $name: goal.name,
                $total: goal.total,
            });
        }
        catch(error) {
            throw error
        }
        
    }

    function all() {
        try {
            
            return database.getAllAsync<GoalResponseDatabas>(`
                SELECT g.id, g.name, g.total, COALESCE(SUM(t.amount), 0) as current
                FROM goals AS g
                LEFT JOIN transactions AS t ON t.goal_id=g.id
                GROUP BY g.id, g.name, g.total
            `);
        }
        catch(error) {
            throw error
        }
    }

    function show(id: number) {
        try {
            
            const statement = database.prepareSync(`
                SELECT g.id, g.name, g.total, COALESCE(SUM(t.amount), 0) as current
                FROM goals AS g
                LEFT JOIN transactions AS t ON t.goal_id=g.id
                WHERE g.id=$id
                GROUP BY g.id, g.name, g.total
            `);

            const result = statement.executeSync<GoalResponseDatabas>({ $id: id })

            return result.getFirstSync();

        }
        catch(error) {
            throw error
        }
    }

    return {
        create,
        all,
        show
    };
}