import { useSQLiteContext } from "expo-sqlite/next";

export type TransactionCreateDatabase = {
    amount: number;
    goalId: number;
}

export type TransactionResponsDatabase = {
    id: string;
    amount: number;
    goal_id: number;
    creted_at: number;
}

export function useTransactionRepository() {
    const database = useSQLiteContext();

    async function findLastest() {
        try {
            const retorno = await  database.getAllAsync<TransactionResponsDatabase>(`
                SELECT * FROM transactions
                ORDER BY created_at DESC LIMIT 10
            `);
            console.log(retorno)
            return retorno;
        }
        catch(error) {
            throw error
        }
    }

    function findByGoal(goalId: number) {
        try {
            
            const statement = database.prepareSync(`
                SELECT *
                FROM transactions
                WHERE goal_id=$goalId
            `);

            const result = statement.executeSync<TransactionResponsDatabase>({ $goalId: goalId })

            return result.getAllSync();

        }
        catch(error) {
            throw error
        }
    }

    function create(transaction: TransactionCreateDatabase) {

        try {
            const statement = database.prepareSync(
                "INSERT INTO transactions (amount, goal_id) VALUES ($amount, $goal_id)"
            );
            statement.executeAsync({
                $amount: transaction.amount,
                $goal_id: transaction.goalId,
            });
        }
        catch(error) {
            throw error
        }
        
    }


    return {
        create,
        findByGoal,
        findLastest
    };
}