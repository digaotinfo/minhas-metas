# Minhas Metas

App desenvovido utilizado Expo, Tailwind


### Para rodar o app utilize os seguintes comandos
```bash
npm i
npx expo start 
``` 

### Sobre o projeto

Imagine que você deseja comprar um Notebook novo que custa R$ 5.000,00. Ele acha prudente poupar o dinheiro necessário para comprar à vista, conseguir um bom preço e não comprometer o orçamento com parcelas.
Por isso, pode usar esse App Minhas Metas para poupar até atingir sua meta financeira para adquirir um Notebook novo.


<h2 style="display: flex; justify-content: center; gap: 10px">
  <img width="150px" alt="Home" title="Home" src=".github/prints/home.png" />

  <img width="150px" alt="Meta" title="Meta" src=".github/prints/meta.png" />
</h2>
